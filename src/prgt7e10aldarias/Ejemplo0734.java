/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Fichero: Ejemplo0734.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-ene-2014
 */
public class Ejemplo0734 {

  public static void mostrar(ArrayList arrayList) {

    System.out.println("LISTADO:");
    // Obtenemos un Iterador y recorremos la lista.
    Iterator iter = arrayList.iterator();
    while (iter.hasNext()) {
      System.out.println(iter.next());
    }
  }

  public static void main(java.lang.String[] args) {

    // Definimos una ArrayList
    ArrayList<String> arrayList;
    arrayList = new ArrayList<>();

    // Añadimos elementos
    arrayList.add("Elemento1");
    arrayList.add("Elemento2");

    // Mostramos la lista
    mostrar(arrayList);

    // Añadir en posición.
    arrayList.add(1, "Elemento3");

    // Mostramos la lista
    mostrar(arrayList);

  }
}
/* EJECUCIÓN:
 LISTADO:
 Elemento1
 Elemento2
 LISTADO:
 Elemento1
 Elemento3
 Elemento2
 */
