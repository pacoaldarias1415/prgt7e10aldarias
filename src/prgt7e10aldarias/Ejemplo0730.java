/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0730.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0730 {

  private static String lista[];
  final static int POS = 3;

  public static void muestra() {
    for (int i = 0; i < POS; i++) {
      System.out.println(lista[i] + " ");
    }
  }

  public static void main(String args[]) {
    lista = new String[POS];
    for (int i = 0; i < POS; i++) {
      System.out.print("String: ");
      String ln = System.console().readLine();
      lista[i] = ln.toString();
    }
    muestra();
    System.out.println("");
  }
}
/* EJECUCION:
 String: abc
 String: def
 String: hij klm
 abc
 def
 hij klm
 */
