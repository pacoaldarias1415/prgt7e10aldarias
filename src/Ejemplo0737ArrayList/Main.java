package Ejemplo0737ArrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-ene-2014
 */
public class Main {

  public static void main(String arg[]) {

    // El tipo es List y lo implementamos con ArrayList
    List<Persona> lp = new ArrayList<Persona>();
    Random r = new Random();
    Persona temp = null;

    int sumaaltura = 0;
    for (int i = 0; i < 1000; i++) {
      lp.add(new Persona(i, "Persona" + i, r.nextInt(100) + 100));
    }

    Iterator<Persona> it = lp.iterator();

    while (it.hasNext()) {
      temp = it.next();
      System.out.println(temp);
      sumaaltura += temp.altura;
    }

    System.out.println("La media de altura del conjunto de Personas es: " + sumaaltura / lp.size());

  }
}
/* EJECUCION;
 Persona-> ID: 998 Nombre: Persona998 Altura: 152
 Persona-> ID: 999 Nombre: Persona999 Altura: 151
 La media de altura del conjunto de Personas es: 149
 * */
